package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.END;
import static ru.sergey.bencode.EscapeSymbols.EOF;
import static ru.sergey.bencode.EscapeSymbols.INTEGER;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * Integers.
 *
 * Integers are encoded as follows: i<integer encoded in base ten ASCII>e The
 * initial i and trailing e are beginning and ending delimiters. You can have
 * negative numbers such as i-3e. Only the significant digits should be used,
 * one cannot pad the Integer with zeroes. such as i04e. However, i0e is valid.
 *
 * Example: i3e represents the integer "3"
 *
 * NOTE: The maximum number of bit of this integer is unspecified, but to handle
 * it as a signed 64bit integer is mandatory to handle "large files" aka
 * .torrent for more that 4Gbyte.
 *
 * @see <a href="https://wiki.theory.org/BitTorrentSpecification#Integers">Bencode types: Integers</a>
 */
public class IntegerSerializer extends TypeSerializer<Number> {

    @Override
    public void serialize(final OutputStream os, final Number value) {
        try {
            os.write(INTEGER);
            os.write(value.toString().getBytes(StandardCharsets.UTF_8));
            os.write(END);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Number deserialize(final InputStream is) {
        final BencodeType type = readType(is, false);

        if (BencodeType.INTEGER != type) {
            throw new SerializationException("Incorrect type");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            int c;

            while ((c = is.read()) != END) {
                if (c == EOF) {
                    throw new SerializationException("Unexpected end of file");
                }

                sb.append((char)c);
            }

            return Long.valueOf(Long.parseLong(sb.toString()));
        } catch (final IOException | NumberFormatException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public boolean canSerialize(final Object value) {
        return value instanceof Integer
            || value instanceof Long
            || value instanceof Short
            || value instanceof Byte;
    }
}
