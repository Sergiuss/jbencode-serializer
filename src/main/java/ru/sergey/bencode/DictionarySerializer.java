package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.DICTIONARY;
import static ru.sergey.bencode.EscapeSymbols.END;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dictionaries
 *
 * Dictionaries are encoded as follows: d<bencoded string><bencoded element>e
 * The initial d and trailing e are the beginning and ending delimiters. Note
 * that the keys must be bencoded strings. The values may be any bencoded type,
 * including integers, strings, lists, and other dictionaries. Keys must be
 * strings and appear in sorted order (sorted as raw strings, not
 * alphanumerics). The strings should be compared using a binary comparison, not
 * a culture-specific "natural" comparison.
 *
 * @see <a href="https://wiki.theory.org/BitTorrentSpecification#Dictionaries" />Bencode types: Dictionaries</a>
 */
public class DictionarySerializer extends TypeSerializer<Map<String, Object>> implements BencodeSerializerAware {
    private BencodeSerializer serializer;

    @Override
    public void serialize(final OutputStream os, final Map<String, Object> value) {
        try {
            os.write(DICTIONARY);

            // sort keys
            final List<String> keys = new ArrayList<>(value.keySet());
            Collections.sort(keys);

            for (int i = 0, n = keys.size(); i < n; i++) {
                final String key = keys.get(i);
                final Object dictValue = value.get(key);

                // serialize key (always byte string)
                serializer.getTypeSerializer(BencodeType.BYTE_STRING).serialize(os, key);
                // serialize value
                serializer.serialize(os, dictValue);
            }

            os.write(END);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Map<String, Object> deserialize(final InputStream is) {
        BencodeType type = readType(is, false);

        if (BencodeType.DICTIONARY != type) {
            throw new SerializationException("Incorrect type");
        }

        final Map<String, Object> dictionary = new HashMap<>();

        String key;
        Object value;

        while ((type = readType(is, true)) != null) {
            // deserialize key (always byte string)
            key = (String) serializer.getTypeSerializer(BencodeType.BYTE_STRING).deserialize(is);
            // deserialize value
            value = serializer.deserialize(is);

            dictionary.put(key, value);
        }

        return dictionary;
    }

    @Override
    public boolean canSerialize(final Object value) {
        return value instanceof Map;
    }

    @Override
    public void setBencodeSerializer(final BencodeSerializer serializer) {
        this.serializer = serializer;
    }
}
