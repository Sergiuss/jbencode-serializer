package ru.sergey.bencode;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * Can serialize all bencode types.
 */
public class BencodeSerializer extends TypeSerializer<Object> {
    private final Map<BencodeType, TypeSerializer<Object>> serializers = new EnumMap<>(BencodeType.class);

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public BencodeSerializer() {
        final TypeSerializer<Object> listSerializer = (TypeSerializer)new ListSerializer();
        final TypeSerializer<Object> dictSerializer = (TypeSerializer)new DictionarySerializer();

        ((BencodeSerializerAware)listSerializer).setBencodeSerializer(this);
        ((BencodeSerializerAware)dictSerializer).setBencodeSerializer(this);

        serializers.put(BencodeType.INTEGER, (TypeSerializer)new IntegerSerializer());
        serializers.put(BencodeType.BYTE_STRING, (TypeSerializer)new ByteStringSerializer());
        serializers.put(BencodeType.LIST, listSerializer);
        serializers.put(BencodeType.DICTIONARY, dictSerializer);
    }

    public BencodeSerializer(final Map<BencodeType, TypeSerializer<Object>> serializers) {
        for (final BencodeType type : BencodeType.values()) {
            final TypeSerializer<Object> serializer = serializers.get(type);

            if (serializer == null) {
                throw new IllegalArgumentException(type.name() + " type serializer is not registred");
            }
            if (serializer instanceof BencodeSerializerAware) {
                ((BencodeSerializerAware)serializer).setBencodeSerializer(this);
            }

            this.serializers.put(type, serializer);
        }
    }

    @Override
    public void serialize(final OutputStream os, final Object value) {
        if (value == null) {
            throw new SerializationException("Null objects is not supported");
        }

        for (final BencodeType type : BencodeType.values()) {
            final TypeSerializer<Object> serializer = serializers.get(type);

            if (serializer.canSerialize(value)) {
                serializer.serialize(os, value);

                return;
            }
        }

        // hack for arrays
        if (value instanceof byte[]) {
            final String strValue = new String((byte[])value, StandardCharsets.UTF_8);
            serializers.get(BencodeType.BYTE_STRING).serialize(os, strValue);

        } else if (value.getClass().isArray()) {
            final List<Object> listValue = Arrays.asList((Object[])value);
            serializers.get(BencodeType.LIST).serialize(os, listValue);

        } else {
            throw new SerializationException("Unsupported class type");
        }
    }

    @Override
    public Object deserialize(final InputStream is) {
        if (!is.markSupported()) {
            throw new SerializationException("InputStream is not supported mark. Use BufferedInputStream as wrapper");
        }

        final BencodeType type = readType(is, true);

        if (type == null) {
            return null;
        }

        return serializers.get(type).deserialize(is);
    }

    public TypeSerializer<Object> getTypeSerializer(final BencodeType type) {
        return serializers.get(type);
    }

    @Override
    public boolean canSerialize(final Object value) {
        return (value != null)
                && (serializers.get(BencodeType.INTEGER).canSerialize(value)
                 || serializers.get(BencodeType.BYTE_STRING).canSerialize(value)
                 || serializers.get(BencodeType.LIST).canSerialize(value)
                 || serializers.get(BencodeType.DICTIONARY).canSerialize(value));
    }
}
