package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.DICTIONARY;
import static ru.sergey.bencode.EscapeSymbols.END;
import static ru.sergey.bencode.EscapeSymbols.EOF;
import static ru.sergey.bencode.EscapeSymbols.INTEGER;
import static ru.sergey.bencode.EscapeSymbols.LENGTH_MAX;
import static ru.sergey.bencode.EscapeSymbols.LENGTH_MIN;
import static ru.sergey.bencode.EscapeSymbols.LIST;
import static ru.sergey.bencode.EscapeSymbols.MARK_LIMIT;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class TypeSerializer<T> {
    public abstract void serialize(final OutputStream os, final T value);

    public abstract T deserialize(final InputStream is);

    public abstract boolean canSerialize(final Object value);

    public BencodeType readType(final InputStream is, final boolean resetInputStream) {
        try {
            if (resetInputStream) {
                is.mark(MARK_LIMIT);
            }

            final int b = is.read();
            final char type = (char)b;

            if (b == EOF || type == END) {
                return null;
            }

            if (resetInputStream) {
                is.reset();
            }

            switch (type) {
            case DICTIONARY:
                return BencodeType.DICTIONARY;
            case INTEGER:
                return BencodeType.INTEGER;
            case LIST:
                return BencodeType.LIST;
            default:
                if (type < LENGTH_MIN || type > LENGTH_MAX) {
                    throw new SerializationException("Unsupported type");
                } else {
                    return BencodeType.BYTE_STRING;
                }
            }
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }
}
