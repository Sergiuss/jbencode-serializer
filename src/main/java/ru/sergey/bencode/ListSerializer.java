package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.END;
import static ru.sergey.bencode.EscapeSymbols.LIST;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Lists.
 *
 * Lists are encoded as follows: l<bencoded values>e The initial l and trailing
 * e are beginning and ending delimiters. Lists may contain any bencoded type,
 * including integers, strings, dictionaries, and even lists within other lists.
 *
 * @see <a href="https://wiki.theory.org/BitTorrentSpecification#Lists">Bencode types: Lists</a>
 */
public class ListSerializer extends TypeSerializer<Iterable<Object>> implements BencodeSerializerAware {
    private BencodeSerializer serializer;

    @Override
    public void serialize(final OutputStream os, final Iterable<Object> value) {
        try {
            os.write(LIST);

            for (final Object listValue : value) {
                serializer.serialize(os, listValue);
            }

            os.write(END);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public Iterable<Object> deserialize(final InputStream is) {
        final BencodeType type = readType(is, false);

        if (BencodeType.LIST != type) {
            throw new SerializationException("Incorrect type");
        }

        final Collection<Object> list = new ArrayList<>();

        Object value;

        while ((value = serializer.deserialize(is)) != null) {
            list.add(value);
        }

        return list;
    }

    @Override
    public boolean canSerialize(final Object value) {
        return value instanceof Iterable;
    }

    @Override
    public void setBencodeSerializer(final BencodeSerializer serializer) {
        this.serializer = serializer;
    }
}
