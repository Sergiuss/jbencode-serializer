package ru.sergey.bencode;

public interface BencodeSerializerAware {
    void setBencodeSerializer(final BencodeSerializer serializer);
}
