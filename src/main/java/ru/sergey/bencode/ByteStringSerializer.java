package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.EOF;
import static ru.sergey.bencode.EscapeSymbols.LENGTH_MAX;
import static ru.sergey.bencode.EscapeSymbols.LENGTH_MIN;
import static ru.sergey.bencode.EscapeSymbols.SEPARATOR;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * Byte strings.
 *
 * A byte string (a sequence of bytes, not necessarily characters) is encoded as
 * <length>:<contents>. The length is encoded in base 10, like integers, but
 * must be non-negative (zero is allowed); the contents are just the bytes that
 * make up the string.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Bencode">Bencode types: Byte
 *      strings</a>
 */
public class ByteStringSerializer extends TypeSerializer<String> {

    @Override
    public void serialize(final OutputStream os, final String value) {
        try {
            os.write(Integer.toString(value.length()).getBytes());
            os.write(SEPARATOR);
            os.write(value.getBytes(StandardCharsets.UTF_8));
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public String deserialize(final InputStream is) {
        final int len = getLength(is);

        try {
            if (is.available() < len) {
                throw new SerializationException("InputStream is smaller than read size");
            }

            final byte[] buffer = new byte[len];

            is.read(buffer, 0, len);

            return new String(buffer, StandardCharsets.UTF_8);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    private int getLength(final InputStream is) {
        try {
            final StringBuilder sb = new StringBuilder();
            int c;

            while ((c = is.read()) != SEPARATOR) {
                if (c == EOF) {
                    throw new SerializationException("Unexpected end of file");
                }

                final char digit = (char)c;

                if (digit < LENGTH_MIN || digit > LENGTH_MAX) {
                    throw new SerializationException("Incorrect type");
                }

                sb.append(digit);
            };

            return Integer.parseInt(sb.toString());
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    @Override
    public boolean canSerialize(final Object value) {
        return value instanceof String;
    }
}
