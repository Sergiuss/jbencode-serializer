package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static ru.sergey.bencode.EscapeSymbols.DICTIONARY;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.Mockito;

public class DictionarySerializerTest {
    private static final String BENCODED_STR = "d4:listl3:addi123ee3:mapd3:key5:value4:listl3:addi123eee5:starti100e3:str6:stringe";

    private final BencodeSerializer bencodeSerializer = new BencodeSerializer();
    private final TypeSerializer<Object> serializer = bencodeSerializer.getTypeSerializer(BencodeType.DICTIONARY);

    @SuppressWarnings("unchecked")
    @Test
    public void dictionarySerializerTest() throws IOException {
        final Map<String, Object> dict = new HashMap<>();
        final Map<String, Object> map = new HashMap<>();
        final List<Object> list = new ArrayList<>();

        list.add("add");
        list.add(123);

        map.put("list", list);
        map.put("key", "value");

        dict.put("start", 100);
        dict.put("list", list);
        dict.put("str", "string");
        dict.put("map", map);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, dict);

        assertEquals(BENCODED_STR, os.toString());

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        final Map<String, Object> desDict = (Map<String, Object>) serializer.deserialize(is);

        os = new ByteArrayOutputStream();

        serializer.serialize(os, desDict);

        assertEquals(BENCODED_STR, os.toString());
    }

    @Test(expected = SerializationException.class)
    public void writeToWrongStream() throws IOException {
        final Map<String, Object> dict = new HashMap<>();
        final OutputStream os = Mockito.mock(OutputStream.class);

        Mockito.doThrow(IOException.class).when(os).write(DICTIONARY);

        serializer.serialize(os, dict);
    }
}
