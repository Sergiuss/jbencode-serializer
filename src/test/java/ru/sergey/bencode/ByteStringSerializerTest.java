package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static ru.sergey.bencode.EscapeSymbols.SEPARATOR;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;
import org.mockito.Mockito;

public class ByteStringSerializerTest {
    private static final String EMPTY_STRING = "";
    private static final String ONE_SYMBOL_STRING = "e";
    private static final String LONG_STRING;

    static {
        final StringBuffer sb = new StringBuffer();

        for (int i = 0; i < 10000; i++) {
            sb.append(i % 10);
        }

        LONG_STRING = sb.toString();
    }

    private final ByteStringSerializer serializer = new ByteStringSerializer();

    @Test
    public void emptyStringTest() throws IOException {
        stringSerializerTest(EMPTY_STRING);
    }

    @Test
    public void oneSymbolStringTest() throws IOException {
        stringSerializerTest(ONE_SYMBOL_STRING);
    }

    @Test
    public void longStringTest() throws IOException {
        stringSerializerTest(LONG_STRING);
    }

    @Test(expected = SerializationException.class)
    public void serializeToClosedStream() throws IOException {
        final OutputStream os = Mockito.mock(OutputStream.class);
        Mockito.doThrow(IOException.class).when(os).write(SEPARATOR);

        serializer.serialize(os, ONE_SYMBOL_STRING);
    }

    @Test(expected = SerializationException.class)
    public void deserializeFromClosedStream() throws IOException {
        final InputStream is = Mockito.mock(InputStream.class);
        Mockito.when(is.read()).thenReturn((int)'1', (int)SEPARATOR);
        Mockito.doThrow(IOException.class).when(is).read(new byte[ONE_SYMBOL_STRING.length()], 0, ONE_SYMBOL_STRING.length());
        Mockito.when(is.available()).thenReturn(ONE_SYMBOL_STRING.length());

        serializer.deserialize(is);
    }

    @Test(expected = SerializationException.class)
    public void deserializeMoreThanAvailableStream() throws IOException {
        final InputStream is = new ByteArrayInputStream("10:".getBytes(StandardCharsets.UTF_8));

        serializer.deserialize(is);
    }

    @Test(expected = SerializationException.class)
    public void getLengthFromClosedStream() throws IOException {
        final InputStream is = Mockito.mock(InputStream.class);
        Mockito.doThrow(IOException.class).when(is).read();

        serializer.deserialize(is);
    }

    @Test(expected = SerializationException.class)
    public void getLengthFromEmptyInputStream() {
        final InputStream is = new ByteArrayInputStream(new byte[0]);

        serializer.deserialize(is);
    }

    @Test(expected = SerializationException.class)
    public void getLengthFromIncorrectInputStream1() {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(';');

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        serializer.deserialize(is);
    }

    private void stringSerializerTest(final String testString) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, testString);

        final InputStream is = new ByteArrayInputStream(os.toByteArray());
        assertEquals(testString, serializer.deserialize(is));
    }
}
