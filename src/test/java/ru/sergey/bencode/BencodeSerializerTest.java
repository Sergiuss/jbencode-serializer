package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.Mockito;

public class BencodeSerializerTest {
    private final BencodeSerializer serializer = new BencodeSerializer();

    @Test
    public void complexTypeTest() {
        final List<Object> objects = new ArrayList<>();
        final Map<String, Object> dict = new HashMap<>();
        final List<Object> list = new ArrayList<>();

        objects.add(1);
        objects.add("test_string");
        objects.add(dict);
        objects.add(new byte[] { 'i', '5', 'e', ':' });
        objects.add(list);
        objects.add("end");
        objects.add(new Object[] { "string", 890L });
        objects.add(new Object[] { "sss", "456" });

        dict.put("list", list);
        dict.put("key", "value");
        dict.put("int", 123);

        list.add("str");
        list.add(999);
        list.add(new Short((short) 5));
        list.add(new Byte((byte) 8));

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, objects);

        final String bencodedString = os.toString();

        final Object obj = serializer.deserialize(new ByteArrayInputStream(os.toByteArray()));

        os = new ByteArrayOutputStream();

        serializer.serialize(os, obj);

        assertEquals(bencodedString, os.toString());
    }

    @Test
    public void decodeTest() {
        final String bencodedString = "d9:publisher3:bob17:publisher-webpage15:www.example.com18:publisher.location4:homee";

        final Object obj = serializer.deserialize(new ByteArrayInputStream(bencodedString.getBytes()));

        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, obj);

        assertEquals(bencodedString, os.toString());
    }

    @Test(expected = SerializationException.class)
    public void serializeNullTest() {
        serializer.serialize(new ByteArrayOutputStream(), (Object) null);
    }

    @Test(expected = SerializationException.class)
    public void serializeUnsupportedType() {
        serializer.serialize(new ByteArrayOutputStream(), new Date());
    }

    @Test
    public void deserializeEmptyStream() {
        final Object value = serializer.deserialize(new ByteArrayInputStream(new byte[0]));
        assertNull(value);
    }

    @Test(expected = SerializationException.class)
    public void deserializeFromClosedStream() throws IOException {
        final InputStream is = Mockito.mock(InputStream.class);
        Mockito.doThrow(IOException.class).when(is).read();

        serializer.deserialize(is);
    }

    @Test
    public void serializerFacadeTest() {
        // byte[]
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, new byte[] { 5, 8, 9 });

        checkResults(os.toString());

        // map
        final Map<String, Object> dict = new HashMap<>();
        dict.put("key", "value");
        dict.put("int", 123);

        os = new ByteArrayOutputStream();

        serializer.serialize(os, dict);

        checkResults(os.toString());

        // int
        os = new ByteArrayOutputStream();

        serializer.serialize(os, 123);

        checkResults(os.toString());

        // long
        os = new ByteArrayOutputStream();

        serializer.serialize(os, 123L);

        checkResults(os.toString());

        // short
        os = new ByteArrayOutputStream();

        serializer.serialize(os, (short)123);

        checkResults(os.toString());

        // byte
        os = new ByteArrayOutputStream();

        serializer.serialize(os, (byte)8);

        checkResults(os.toString());
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void customSerializerRegistration() {
        final Map<BencodeType, TypeSerializer<Object>> serializers = new EnumMap<>(BencodeType.class);

        serializers.put(BencodeType.INTEGER, (TypeSerializer)new IntegerSerializer());
        serializers.put(BencodeType.BYTE_STRING, (TypeSerializer)new ByteStringSerializer());
        serializers.put(BencodeType.LIST, (TypeSerializer)new ListSerializer());
        serializers.put(BencodeType.DICTIONARY, (TypeSerializer)new DictionarySerializer());

        final BencodeSerializer bencodeSerializer = new BencodeSerializer(serializers);

        final String bencodedString = "d4:listl3:addi123ee3:mapd3:key5:value4:listl3:addi123eee5:starti100e3:str6:stringe";

        final Object deserializedObj = bencodeSerializer.deserialize(new ByteArrayInputStream(bencodedString.getBytes()));

        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        bencodeSerializer.serialize(os, deserializedObj);

        assertEquals(bencodedString, os.toString());
    }

    private void checkResults(final String bencodedString) {
        final Object deserializedObj = serializer.deserialize(new ByteArrayInputStream(bencodedString.getBytes()));

        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, deserializedObj);

        assertEquals(bencodedString, os.toString());
    }
}
