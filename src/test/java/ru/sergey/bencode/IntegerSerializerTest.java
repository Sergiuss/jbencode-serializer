package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static ru.sergey.bencode.EscapeSymbols.INTEGER;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;
import org.mockito.Mockito;

public class IntegerSerializerTest {
    private final IntegerSerializer serializer = new IntegerSerializer();

    @Test
    public void integerSerializerTest() throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        serializer.serialize(os, Long.MIN_VALUE);
        serializer.serialize(os, Long.MAX_VALUE);
        serializer.serialize(os, 0);

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        assertEquals(Long.MIN_VALUE, serializer.deserialize(is));
        assertEquals(Long.MAX_VALUE, serializer.deserialize(is));
        assertEquals(0L, serializer.deserialize(is));
    }

    @Test(expected = SerializationException.class)
    public void emptyInputStreamDeserialize() {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(INTEGER);

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        serializer.deserialize(is);
    }

    @Test(expected = SerializationException.class)
    public void serializeToClosedStream() throws IOException {
        final OutputStream os = Mockito.mock(OutputStream.class);
        Mockito.doThrow(IOException.class).when(os).write(INTEGER);

        serializer.serialize(os, Long.MIN_VALUE);
    }

    @Test(expected = SerializationException.class)
    public void deserializeFromClosedStream() throws IOException {
        final InputStream is = Mockito.mock(InputStream.class);
        Mockito.doThrow(IOException.class).when(is).read();

        serializer.deserialize(is);
    }
}
